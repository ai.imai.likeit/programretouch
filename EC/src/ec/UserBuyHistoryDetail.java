package ec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBuyDetailBeans;
import dao.BuyDAO;
import dao.BuyDetailDAO;

/**
 * 購入履歴画面
 * @author d-yamaguchi
 *
 */
@WebServlet("/UserBuyHistoryDetail")
public class UserBuyHistoryDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		/*セッション開始*/
		HttpSession session = request.getSession();
		int userId = (int) session.getAttribute("userId");

		/*URLからGetパラメータとして、IDをint型にして受け取る*/
		String buyId = request.getParameter("buy_id");
		int buyIdInt = Integer.parseInt(buyId);

		/*確認用*/
		System.out.println(buyIdInt);

		/*userId,buyIdIntを引数にして紐づくデータを出力
		 * 購入日時,配送方法,合計金額*/
		BuyDAO buyDao = new BuyDAO();
		UserBuyDetailBeans buyDateDeliMethodTotalPrice =buyDao.getBuyDateBeransByUserIdBuyId(userId, buyIdInt);

		/*商品名,単価*/
		BuyDetailDAO buyDetailDao = new BuyDetailDAO();
		ArrayList<UserBuyDetailBeans> itemAndDeliveryMethod = buyDetailDao.getUserBuyDetailDataList(userId, buyIdInt);

		/*配送方法,配送金額*/
		UserBuyDetailBeans userBuyDeliveryMethod = buyDao.getUserBuyDeliveryMethod(userId, buyIdInt);

		request.setAttribute("buyDDMTP", buyDateDeliMethodTotalPrice);
		request.setAttribute("itemADM", itemAndDeliveryMethod);
		request.setAttribute("buyDeliveryMethod", userBuyDeliveryMethod);
		request.getRequestDispatcher(EcHelper.USER_BUY_HISTORY_DETAIL_PAGE).forward(request, response);

	}
}
