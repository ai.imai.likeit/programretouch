package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import base.DBManager;
import beans.BuyDataBeans;
import beans.UserBuyDetailBeans;

/**
 *
 * @author d-yamaguchi
 *
 */
public class BuyDAO {


	/**
	 * 購入情報登録処理
	 * @param bdb 購入情報
	 * @throws SQLException 呼び出し元にスローさせるため
	 */
	public static int insertBuy(BuyDataBeans bdb) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		int autoIncKey = -1;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"INSERT INTO t_buy(user_id,total_price,delivery_method_id,create_date) VALUES(?,?,?,?)",
					Statement.RETURN_GENERATED_KEYS);
			st.setInt(1, bdb.getUserId());
			st.setInt(2, bdb.getTotalPrice());
			st.setInt(3, bdb.getDelivertMethodId());
			st.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
			st.executeUpdate();

			ResultSet rs = st.getGeneratedKeys();
			if (rs.next()) {
				autoIncKey = rs.getInt(1);
			}
			System.out.println("inserting buy-datas has been completed");

			return autoIncKey;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 購入IDによる購入情報検索
	 * @param buyId
	 * @return BuyDataBeans
	 * 				購入情報のデータを持つJavaBeansのリスト
	 * @throws SQLException
	 * 				呼び出し元にスローさせるため
	 */
	public static BuyDataBeans getBuyDataBeansByBuyId(int buyId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT * FROM t_buy"
							+ " JOIN m_delivery_method"
							+ " ON t_buy.delivery_method_id = m_delivery_method.id"
							+ " WHERE t_buy.id = ?");
			st.setInt(1, buyId);

			ResultSet rs = st.executeQuery();

			BuyDataBeans bdb = new BuyDataBeans();
			if (rs.next()) {
				bdb.setId(rs.getInt("id"));
				bdb.setTotalPrice(rs.getInt("total_price"));
				bdb.setBuyDate(rs.getTimestamp("create_date"));
				bdb.setDelivertMethodId(rs.getInt("delivery_method_id"));
				bdb.setUserId(rs.getInt("user_id"));
				bdb.setDeliveryMethodPrice(rs.getInt("price"));
				bdb.setDeliveryMethodName(rs.getString("name"));
			}

			System.out.println("searching BuyDataBeans by buyID has been completed");

			return bdb;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/*UserDataの購入日時と配送方法と合計金額*/
	public ArrayList<UserBuyDetailBeans> getBuyDataBeansList(int userId){

		Connection con = null;
		ArrayList<UserBuyDetailBeans> buyDataList = new ArrayList<UserBuyDetailBeans>();

		try {
			con = DBManager.getConnection();

			String sql = "SELECT "
					+ "t_buy.id AS id,"
					+ "t_buy_detail.buy_id AS buyId,"
					+ "t_buy.user_id AS userId,"
					+ "t_buy.total_price AS totalPrice,"
					+ "t_buy.create_date AS createDate,"
					+ "m_delivery_method.name AS deliveryMethodName,"
					+ "m_delivery_method.price AS deliveryMethodPrice "
					+ "FROM t_buy "
					+ "INNER JOIN m_delivery_method ON t_buy.delivery_method_id = m_delivery_method.id "
					+ "INNER JOIN t_buy_detail ON t_buy.id = t_buy_detail.buy_id "
					+ "WHERE t_buy.user_id = ? "
					+ "GROUP BY ec_db.t_buy_detail.buy_id";

			PreparedStatement pStml = con.prepareStatement(sql);
			pStml.setInt(1, userId);
			ResultSet rs = pStml.executeQuery();

			while (rs.next()) {
				int idData = rs.getInt("id");
				int buyIdData= rs.getInt("buyId");
				int userIdData = rs.getInt("userId");
				int totalPriceData = rs.getInt("totalPrice");
				Date buyDateData = rs.getTimestamp("createDate");
				String deliveryMethodNameData = rs.getString("deliveryMethodName");
				int deliveryMethodPriceData = rs.getInt("deliveryMethodPrice");

				UserBuyDetailBeans buyData = new UserBuyDetailBeans(idData,buyIdData,userIdData,totalPriceData,
						buyDateData,deliveryMethodNameData,deliveryMethodPriceData);

				buyDataList.add(buyData);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return buyDataList;

	}

	/*購入履歴詳細の購入日時と配送方法と合計金額*/
	public UserBuyDetailBeans getBuyDateBeransByUserIdBuyId(int userId, int buyId) {

		Connection conn = null;
		UserBuyDetailBeans uBDB = new UserBuyDetailBeans();

		try {
			conn = DBManager.getConnection();


			String sql ="SELECT "
					+ "t_buy.total_price AS totalPrice,"
					+ "t_buy.create_date AS buyDate,"
					+ "m_delivery_method.name AS deliveryMethodName "
					+ "FROM t_buy "
					+ "INNER JOIN m_delivery_method "
					+ "ON t_buy.delivery_method_id = m_delivery_method.id "
					+ "INNER JOIN t_buy_detail "
					+ "ON t_buy.id = t_buy_detail.buy_id "
					+ "WHERE t_buy.user_id = ? AND t_buy_detail.buy_id = ? "
					+ "GROUP BY t_buy_detail.buy_id";

			PreparedStatement pStml = conn.prepareStatement(sql);
			pStml.setInt(1, userId);
			pStml.setInt(2, buyId);
			ResultSet rs = pStml.executeQuery();

			if (!rs.next()) {
				return null;
			}

			uBDB.setTotalPrice(rs.getInt("totalPrice"));
			uBDB.setBuyDate(rs.getTimestamp("buyDate"));
			uBDB.setDeliveryMethodName(rs.getString("deliveryMethodName"));

		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			if (conn!=null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return uBDB;
	}

	public UserBuyDetailBeans getUserBuyDeliveryMethod(int userId, int buyId) {

		Connection conn = null;
		UserBuyDetailBeans uBDB = new UserBuyDetailBeans();

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT "
					+ "m_delivery_method.name AS deliveryMethodName,"
					+ "m_delivery_method.price AS deliveryMethodPrice "
					+ "FROM t_buy "
					+ "INNER JOIN m_delivery_method "
					+ "ON t_buy.delivery_method_id = m_delivery_method.id "
					+ "INNER JOIN t_buy_detail "
					+ "ON t_buy.id = t_buy_detail.buy_id "
					+ "WHERE t_buy.user_id = ? AND t_buy_detail.buy_id = ? "
					+ "GROUP BY t_buy_detail.buy_id";

			PreparedStatement pStml = conn.prepareStatement(sql);
			pStml.setInt(1, userId);
			pStml.setInt(2, buyId);
			ResultSet rs = pStml.executeQuery();

			if (!rs.next()) {
				return null;
			}

			uBDB.setDeliveryMethodName(rs.getString("deliveryMethodName"));
			uBDB.setDeliveryMethodPrice(rs.getInt("deliveryMethodPrice"));

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return uBDB;
	}
}

