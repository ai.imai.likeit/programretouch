package beans;

import java.text.SimpleDateFormat;
import java.util.Date;

public class UserBuyDetailBeans {

	private int id;
	private int buyId;
	private int userId;
	private String itemName;
	private int itemPrice;
	private int totalPrice;
	private Date buyDate;
	private String deliveryMethodName;
	private int deliveryMethodPrice;

	public UserBuyDetailBeans() {

	}

	public UserBuyDetailBeans(int userId) {
		this.userId = userId;
	}

	public UserBuyDetailBeans(int userId, int buyId) {
		this.userId = userId;
		this.buyId = buyId;
	}

	public UserBuyDetailBeans(int totalPrice, Date buyDate, String deliveryMethodName) {
		this.totalPrice = totalPrice;
		this.buyDate = buyDate;
		this.deliveryMethodName = deliveryMethodName;
	}

	public UserBuyDetailBeans(int id, int buyId, int userId, String itemName, int itemPrice) {
		this.id = id;
		this.buyId = buyId;
		this.userId = userId;
		this.itemName = itemName;
		this.itemPrice = itemPrice;
	}

	public UserBuyDetailBeans(int id, int buyId, int userId, int totalPrice, Date buyDate,
			String deliveryMethodName, int deliveryMethodPrice) {
		this.id = id;
		this.buyId = buyId;
		this.userId = userId;
		this.totalPrice = totalPrice;
		this.buyDate = buyDate;
		this.deliveryMethodName = deliveryMethodName;
		this.deliveryMethodPrice = deliveryMethodPrice;

	}

	public int getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(int itemPrice) {
		this.itemPrice = itemPrice;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getBuyId() {
		return buyId;
	}

	public void setBuyId(int buyId) {
		this.buyId = buyId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public int getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Date getBuyDate() {
		return buyDate;
	}

	public void setBuyDate(Date buyDate) {
		this.buyDate = buyDate;
	}

	public String getDeliveryMethodName() {
		return deliveryMethodName;
	}

	public void setDeliveryMethodName(String deliveryMethodName) {
		this.deliveryMethodName = deliveryMethodName;
	}

	public int getDeliveryMethodPrice() {
		return deliveryMethodPrice;
	}

	public void setDeliveryMethodPrice(int deliveryMethodPrice) {
		this.deliveryMethodPrice = deliveryMethodPrice;
	}

	public String getFormatDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日HH時mm分");
		return sdf.format(buyDate);
	}

}
